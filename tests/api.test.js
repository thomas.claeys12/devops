test("api call should return Hello Sciences-U !", async () => {
  const apiCall = await fetch("/hello", { method: "GET" });
  const response = await apiCall.json();

  console.log(response);
});
