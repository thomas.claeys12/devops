import express from "express";

const app = express();

app.get("/hello", (req, res) => {
  return res.send("Hello Sciences-U !");
});

app.listen(3000, () => console.log("listening..."));
